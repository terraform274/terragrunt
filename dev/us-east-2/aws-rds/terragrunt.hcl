include {
  path = find_in_parent_folders()
}

locals {
  # Automatically load environment-level variables
  region_vars       = read_terragrunt_config(find_in_parent_folders("region.hcl"))
  account_vars      = read_terragrunt_config(find_in_parent_folders("account.hcl"))

  
  # Extract out common variables for reuse
  environment       = local.account_vars.locals.environment
  aws_region        = local.region_vars.locals.aws_region
}

terraform {
  source = "github.com/terraform-aws-modules/terraform-aws-rds//."
}

inputs = {

  identifier = "db-dev"
  skip_final_snapshot = true

  
  create_db_subnet_group = true
  subnet_ids             = ["subnet-036228eaa741a70f2", "subnet-006037cd225542a53"]

  engine            = "mysql"
  engine_version    = "5.7"
  instance_class    = "db.t3.micro"
  allocated_storage = 5

  db_name  = "dbqa"
  username = "user"
  port     = "3306"

  iam_database_authentication_enabled = true

  vpc_security_group_ids = ["sg-0fba99356fec9cc52"]

  
 
 

  tags = {
    Owner       = "user"
    Environment = "qa"
  }

  
  # DB parameter group
  family = "mysql5.7"

  # DB option group
  major_engine_version = "5.7"

  # Database Deletion Protection
  deletion_protection = false

  

  
}

prevent_destroy = false
