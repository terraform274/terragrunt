# Run the Terragrunt Code for dev

```
cd dev
terragrunt run-all init
terragrunt run-all plan
terragrunt run-all apply
terragrunt run-all destroy  (For destroy The infrastructure)
```

# Run the Terragrunt Code for prod

```
cd prod
terragrunt run-all init
terragrunt run-all plan
terragrunt run-all apply
terragrunt run-all destroy  (For destroy The infrastructure)
```

# Run the Terragrunt Code for qa

```
cd qa
terragrunt run-all init
terragrunt run-all plan
terragrunt run-all apply
terragrunt run-all destroy  (For destroy The infrastructure)
```

# Run the Terragrunt Code for stage

```
cd stage
terragrunt run-all init
terragrunt run-all plan
terragrunt run-all apply
terragrunt run-all destroy  (For destroy The infrastructure)
```




