include {
  path = find_in_parent_folders()
}

locals {
  # Automatically load environment-level variables
  region_vars       = read_terragrunt_config(find_in_parent_folders("region.hcl"))
  account_vars      = read_terragrunt_config(find_in_parent_folders("account.hcl"))

  
  # Extract out common variables for reuse
  environment       = local.account_vars.locals.environment
  aws_region        = local.region_vars.locals.aws_region
}

terraform {
  source = "github.com/terraform-aws-modules/terraform-aws-rds//."
}

inputs = {
    skip_final_snapshot = true

  identifier = "db-beta"

  engine            = "mysql"
  engine_version    = "5.7"
  instance_class    = "db.t3.micro"
  allocated_storage = 5

  db_name  = "dbprod"
  username = "user"
  port     = "3306"

  iam_database_authentication_enabled = true

  # vpc_security_group_ids = ["sg-0c2efa52e5db7e72c"]

  # maintenance_window = "Mon:00:00-Mon:03:00"
  # backup_window      = "03:00-06:00"
  # monitoring_interval = "30"
  # monitoring_role_name = "MyRDSMonitoringRole"
  # create_monitoring_role = true

  tags = {
    Owner       = "user"
    Environment = "prod"
  }

 
  # DB parameter group
  family = "mysql5.7"

  # DB option group
  major_engine_version = "5.7"

  # Database Deletion Protection
  deletion_protection = false

  create_db_subnet_group = true
  subnet_ids             = ["subnet-081a3a11f29d77f43", "subnet-080d1877094ae9acd"]


}

prevent_destroy = false